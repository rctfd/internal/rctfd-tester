## Version v0.1.2 Release Notes

- Add [tarpaulin][tarpaulin] for code coverage

[tarpaulin]: https://github.com/xd009642/tarpaulin

## Version v0.1.1

- Add [mdbook][mdbook]

[mdbook]: https://github.com/rust-lang/mdBook

## Version v0.1.0

- Initialise!
- Install [Rust][rust], [Clippy][clippy], and [cargo-audit][cargo-audit] 
- Add Addison as a maintainer 

[rust]: https://www.rust-lang.org/
[clippy]: https://github.com/rust-lang/rust-clippy
[cargo-audit]: https://github.com/RustSec/cargo-audit
