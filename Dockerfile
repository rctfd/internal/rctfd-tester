FROM debian:buster-slim
LABEL maintainer="Addison Crump <me@addisoncrump.info>"

# install Rust dependencies
RUN apt update
RUN apt -y install build-essential curl git libssl-dev pkg-config
RUN apt clean autoclean && apt -y autoremove && bash -c "rm -rf /var/lib/{apt,dpkg,cache,log}/"

# install Rust
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --profile minimal -y
ENV PATH="/root/.cargo/bin:${PATH}"
RUN rustup toolchain install nightly

# install dependencies required for testing
RUN rustup component add clippy
RUN cargo install cargo-tarpaulin
RUN cargo install cargo-audit
RUN cargo install mdbook --no-default-features --features output
