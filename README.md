# RCTFd Test Container

This repository holds the base docker image build files required for all RCTFd
Rust-based repositories.

## Version (v0.1.2) Release Notes

- Add [tarpaulin][tarpaulin] for code coverage

[tarpaulin]: https://github.com/xd009642/tarpaulin
